Bootstrap_Simple is , as name suggested a simple boostrap3 themes.
It is a fully responsive template can be used for creating clean design agency website. 
The front page banner and texts can easily be changed from theme settings page. 
Visit - /admin/appearance and select bootstrap simple theme .
 You will be presented with banner setting and welcome content setting form.
 Enter the content as you need and save.

This html5 template comes with following regions:

header: 'Header'
  primary_menu: 'Primary Menu'
  main_slider: 'Main Slider'  
  welcome_text: 'Welcome Text'  
  topwidget_first: 'Top First Widget'
  topwidget_second: 'Top Second Widget'
  topwidget_third: 'Top Third Widget'
  breadcrumb: 'Breadcrumb'
  content: 'Content'
  bottom_widget_first: 'Bottom First Widget'
  bottom_widget_second: ' Bottom Second Widget'
  bottom_widget_third: ' Bottom Third Widget'
  bottom_widget_fourth: ' Bottom Fourth Widget'
  footer_first_widget: 'Footer First Widget'
  footer_second_widget: 'Footer Second Widget'
  footer: 'Footer'
